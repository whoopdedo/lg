#include "lg/config.h"
#include "lg/objstd.h"
#include "lg/interfaceimp.h"
#ifdef _MT
# include <windows.h>
uint cRefCnt::IncRef()
{
	return InterlockedIncrement((LONG*)&m_iRef);
}

uint cRefCnt::DecRef()
{
	uint ref = m_iRef;
	if (ref > 0)
	{
		ref = InterlockedDecrement((LONG*)&m_iRef);
	}
	return ref;
}
#endif
